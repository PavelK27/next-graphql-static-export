// New release features:
// Overview: Designed to solve the problem of creating Wordpress category and tag pages, but the API has built-in
// parameters so that this schema is not assumed
// 1. Now handles WP-style category and tag pages
// 2. Handles custom queries - these are passed on to the template / component
// 3. You can provide custom URL parameters and schema drill down data
// 4. Option to flatten nested category URLs
// 5. Option to skip URLs: either standard pages or categories
//  -- assigned at the "type" level
const fs = require("fs");
const { format } = require("date-fns");
const { request } = require("graphql-request");

// Hat tip to @jasonbahl, for the original fetchPosts
// here: https://github.com/wp-graphql/gatsby-wpgraphql-blog-example
const fetchPosts = async (
  variables,
  { query, contentType, endpoint, perPage, parseQueryResults },
  list,
  pageNumber
) =>
  await request(endpoint, query, variables).then(res => {
    // I like console.logs, you can of course remove..
    console.log(`fetching ${contentType} page ${pageNumber}`);

    // Interpolate the post type passed in with [type]
    // and allow for passing a custom parsing function
    let nodes, hasNextPage, endCursor;
    if (parseQueryResults) {
      ({ nodes, hasNextPage, endCursor } = parseQueryResults(res, contentType));
    } else {
      ({
        [contentType]: {
          nodes,
          pageInfo: { hasNextPage, endCursor }
        }
      } = res);
    }
    console.log(`Fetched ${nodes.length} pieces of content`);
    console.log("Cursor", res[contentType].pageInfo.endCursor);

    // Pull the post on to the master list for this post type (post, page, product, etc.)
    nodes.map(post => {
      list.push(post);
    });

    // Are there more pages of posts? If so, use the endCursor from the
    // previous query to run another query until we're out of more content
    if (hasNextPage) {
      pageNumber++;
      return fetchPosts(
        { first: perPage, after: endCursor },
        { query, contentType, endpoint, perPage, parseQueryResults },
        list,
        pageNumber,
        endpoint
      );
    }
    return list;
  });

// Kick things off to the central workhorse
const processContent = async typeParams => {
  const promises = [];

  // Grab our content
  typeParams.forEach(type => promises.push(populateNextExportData(type)));
  const results = await Promise.all(promises);
  return results;
};

// Our central "workhorse"
const populateNextExportData = async params => {
  // Destructure for tidiness
  const {
    urlBase,
    urlExtensionParameters,
    parameterizeQueryValues,
    insertParameter,
    flattenNestedUrlsOn,
    routeQuery,
    pageComponent,
    perPage,
    includeDate,
    dateParam,
    skipByParameter,
    placePagesAtRoot,
    specialUrls,
    skip
  } = params;
  let urlExtension = urlExtensionParameters ? urlExtensionParameters : "uri";

  const pageNumber = 0;
  const list = new Array();

  // Fire off the first query, starting on the first page (after: null)
  const results = await fetchPosts(
    { first: perPage, after: null },
    params,
    list,
    pageNumber
  ).then(results => {
    // Once all pages have complete, give Next the structure of data it needs
    const content = results.reduce((items, item) => {
      let skipThis = false;
      // urlExtensionParameters.map(param => {
      //   if (skip.includes(item[param])) {
      //     skipThis = true;
      //   }
      // });

      // Handle skipping URLs by data on the GraphQL object
      if (skipByParameter) {
        skipByParameter.map(param => {
          if (item[param]) {
            skipThis = true;
          }
        });
        // If we should skip, just return the previous items
      }

      if (skip && skip.uriParam && skip.urls.includes(item[skip.uriParam])) {
        skipThis = true;
      }
      if (skipThis) return items;

      // Create the URL that we will pin the data to which gets returned to Next.js export
      let url = createUrl(
        item,
        urlBase,
        urlExtension,
        insertParameter,
        flattenNestedUrlsOn,
        placePagesAtRoot,
        specialUrls
      );

      let processedPageComp;

      if (isFunction(pageComponent)) {
        processedPageComp = pageComponent(item);
      } else {
        processedPageComp = pageComponent;
      }

      // Structure the data the Next.js needs to export data with templates
      const fun = Object.assign({}, items, {
        [url]: {
          // the url we want to give this page
          page: `/${processedPageComp}`, // "page" here refers to the component in ./pages
          query: extractQueryParams(
            // data to pass to the component so it has content
            item,
            routeQuery,
            parameterizeQueryValues,
            urlExtensionParameters
          ),
          modified: getItemModified(item, includeDate, dateParam) // for sitemaps
        }
      });

      return fun;
    }, {});

    return content;
  });

  return results;
};

// Some fancy property drilling
const resolve = (path, obj) => {
  // Config-declared data and index
  if (path.type === "array") {
    return `${obj[path.param][path.index]}/${[obj[path.uriParam]]}`;
  }

  // Config-declared string representation of object parameters
  if (path.type === "string") {
    return path.param.split(".").reduce(function(prev, curr) {
      return prev ? prev[curr] : null;
    }, obj || self);
  }

  // Nothing fancy, just return
  return obj[path];
};

const createUrl = (
  item,
  urlBase,
  urlExtension,
  insertParameter,
  flattenNestedUrlsOn,
  placePagesAtRoot,
  specialUrls
) => {
  // If the config passes us a special URL, there is no point in doing additional processing
  // As the config is telling us the exact URL we should end up with
  // So, just follow their instructions
  let specialUrl;

  // TEST
  if (specialUrls && specialUrls.urls) {
    specialUrls.urls.map(url => {
      if (item[specialUrls["uriParam"]] === url.uri) {
        specialUrl = url.finalUrl;
      }
    });
    if (specialUrl) {
      return specialUrl;
    }
  }

  // Other than that...

  // Default: false, as we should be conservative about changing URLs
  // TEST
  if (flattenNestedUrlsOn !== undefined && !flattenNestedUrlsOn) {
    if (item.parent) {
      if (insertParameter) {
        return `/${urlBase}/${resolve(insertParameter, item)}/${
          item[urlExtension]
        }`;
      }
      // Yes there is a parent. No custom parent parameter
      return `/${urlBase}/${item.parent.slug}/${item[urlExtension]}`;
    }
  }

  // TESTED
  if (flattenNestedUrlsOn) {
    return `/${urlBase}/${resolve(flattenNestedUrlsOn, item)}`;
  }

  // Place the url at the root of the site?
  // TEST
  if (placePagesAtRoot) {
    return `/${resolve(urlExtension, item)}`;
  }

  // Insert parameterized data in to the URL structure
  // TEST
  if (insertParameter) {
    return `/${urlBase}/${resolve(insertParameter, item)}`;
  }

  // Flatten is true
  // TEST
  let extension = resolve(urlExtension, item);
  let url = `/${urlBase}/${extension}`;

  return url;
};

const isFunction = functionToCheck => {
  return (
    functionToCheck && {}.toString.call(functionToCheck) === "[object Function]"
  );
};

const extractQueryParams = (
  item,
  routeQuery,
  parameterizeQueryValues,
  urlExtensionParameters
) => {
  let query;

  // The default query for "pages", "posts", or custom post types.
  // If no initial query is passed in via params, and no parameter arrays are passed in,
  // we'll just use this sensible default. You can completely override this value by passing
  // your own query, which will be passed all the way to your templates / components.
  if (
    routeQuery === undefined &&
    !(parameterizeQueryValues && urlExtensionParameters)
  ) {
    query = { uri: item.uri };
  }

  // Establish the query using the routeQuery, if there is one
  if (routeQuery) {
    // Yes, we are overriding the previous value
    query = routeQuery(item);
  }

  // Map over the parameters from the config and add them to the query
  if (parameterizeQueryValues && urlExtensionParameters) {
    parameterizeQueryValues.map((value, i) => {
      query[value] = item[urlExtensionParameters[i]];
    });
  }

  return query;
};

// Get the date that this item was modified. Useful for generating sitemaps.
const getItemModified = (item, includeDate, dateParam) => {
  // If they don't want the date returned, bail
  if (!includeDate) return;
  // If they don't include a special date parameter name, we'll just use 'modified'
  if (!dateParam) {
    // Make sure this parameter exists on the object, and if it does, return this
    if (item["modified"]) {
      return item["modified"];
    }
    // If the parameter does not exist on the object, return the current date / time
    return Date.now();
  }
  // The default case is that we will return the items custom dataParam
  if (item[dateParam]) {
    return item[dateParam];
  }
  // Otherwise, just return the current date
  return Date.now();
};

const generateSitemapData = data => {
  // Hat tip: @embiem: https://dev.to/embiem/auto-generate-sitemapxml-in-nextjs-2nh1
  const siteMapData = {};

  // Structure the data as needed
  for (const key in data) {
    let value = data[key];

    if (data.hasOwnProperty(key)) {
      // Not a property from prototype chain
      siteMapData[`${key}`] = {
        page: `${key}`,
        lastModified: value.modified
          ? format(value.modified)
          : format(Date.now())
      };
    }
  }

  return siteMapData;
};

const generateXmlSitemaps = (
  site,
  result,
  sitemapSize,
  sitemapFileRoot,
  sitemapStylesFile
) => {
  const sitemapData = generateSitemapData(result);
  let initialData = [];

  // Turn the data object in to an array
  for (let item in sitemapData) {
    if (sitemapData.hasOwnProperty(item)) {
      initialData.push(sitemapData[item]);
    }
  }

  // 1. Split data object in to <sitemapSize> sized arrays
  // Will be structured like [[stuff, stuff], [stuff, stuff], [stuff, stuff]]
  // Then operate on it like dataGroups.map...
  const data2D = arrayTo2DArray(initialData, sitemapSize);

  // Generate the child sitemaps
  let childSitemaps = data2D.map(group => {
    return createChildSitemap(site, group, sitemapStylesFile);
  });

  // array of child sitemap names that we can push to the master sitemap
  const sitemapFileNames = [];

  // Write the child sitemaps to disk
  childSitemaps.map((sitemap, i) => {
    let fileName;
    if (sitemap) {
      if (childSitemaps.length) {
        fileName = `${sitemapFileRoot}-${i}.xml`;
        fs.writeFileSync(`out/${fileName}`, sitemap);
        sitemapFileNames.push(fileName);
      } else {
        fileName = `${sitemapFileRoot}.xml`;
        fs.writeFileSync(`out/${fileName}`, sitemap);
        sitemapFileNames.push(fileName);
      }
      console.log(`Sitemap ${fileName} created!`);
    }
    return;
  });

  // 5. Create the parent sitemap, linking to each child sitemap
  const masterSitemap = createMasterSitemap(
    site,
    sitemapFileNames,
    sitemapStylesFile
  );
  fs.writeFileSync("out/sitemap_index.xml", masterSitemap);
  console.log("Master sitemap written!");

  return;
};

const createMasterSitemap = (site, data, sitemapStylesFile) => {
  const sitemapXml = `<?xml version="1.0" encoding="UTF-8"?>${
    sitemapStylesFile
      ? `<?xml-stylesheet type="text/xsl" href="${site}/${sitemapStylesFile}"?>`
      : ""
  }
  <sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
  ${data
    .map(
      path => `
  <sitemap>
    <loc>${site}/${path}</loc>
    <lastmod>${format(Date.now())}</lastmod>
  </sitemap>`
    )
    .join("")}
  </sitemapindex>`;
  return sitemapXml;
};

const createChildSitemap = (site, data, sitemapStylesFile) => {
  const sitemapXml = `<?xml version="1.0" encoding="UTF-8"?>${
    sitemapStylesFile
      ? `<?xml-stylesheet type="text/xsl" href="${site}/${sitemapStylesFile}"?>`
      : ""
  }
  <urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd http://www.google.com/schemas/sitemap-image/1.1 http://www.google.com/schemas/sitemap-image/1.1/sitemap-image.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
  ${data
    .map(({ page, lastModified }) => {
      let processedSite = site;
      let processedPage;

      // Take care of conflicts that would return https://site.com//blah
      if (site.charAt(site.length - 1) === "/" && page.charAt(0) === "/") {
        processedSite = site.slice(0, -1);
      }
      processedPage = page.replace("//", "/");

      return `
        <url>
          <loc>${processedSite}${processedPage}</loc>
          <lastmod>${lastModified}</lastmod>
        </url>`;
    })
    .join("")}
  </urlset>`;

  return sitemapXml;
};

// hattip: https://blog.abelotech.com/posts/array-conversion-2-dimensional-javascript/
function arrayTo2DArray(list, howMany) {
  var idx = 0;
  result = [];

  while (idx < list.length) {
    if (idx % howMany === 0) result.push([]);
    result[result.length - 1].push(list[idx++]);
  }

  return result;
}

module.exports = {
  processContent,
  generateXmlSitemaps
};
